# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/gauss_pyramid.c" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/gauss_pyramid.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"visual_odometry\""
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/devel/include"
  "/usr/include/eigen3"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/depth_image.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/depth_image.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/fast.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/fast.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/feature_matcher.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/feature_matcher.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/frame.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/frame.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/grid_filter.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/grid_filter.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/initial_homography_estimation.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/initial_homography_estimation.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/intensity_descriptor.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/intensity_descriptor.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/internal_utils.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/internal_utils.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/motion_estimation.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/motion_estimation.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/normalize_image.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/normalize_image.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/primesense_depth.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/primesense_depth.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/pyramid_level.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/pyramid_level.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/rectification.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/rectification.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/refine_feature_match.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/refine_feature_match.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/refine_motion_estimate.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/refine_motion_estimate.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/stereo_depth.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/stereo_depth.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/stereo_frame.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/stereo_frame.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/stereo_rectify.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/stereo_rectify.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/tictoc.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/tictoc.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis/visual_odometry.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/3rd_party/fovis/libfovis/visual_odometry.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/src/main.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/visual_odometry/CMakeFiles/visual_odometry.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"visual_odometry\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/devel/include"
  "/usr/include/eigen3"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/visual_odometry/3rd_party/fovis/libfovis"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
