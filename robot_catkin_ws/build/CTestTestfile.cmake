# CMake generated Testfile for 
# Source directory: /home/tania/Documents/ur5_robot/robot_catkin_ws/src
# Build directory: /home/tania/Documents/ur5_robot/robot_catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("ur_description")
subdirs("ground_plane_estimation")
subdirs("move_model/move_model")
subdirs("robot_controller")
subdirs("upper_body_detector")
subdirs("ur5_robot")
subdirs("ur5_robot_gazebo")
subdirs("visual_odometry")
subdirs("mdl_people_tracker")
subdirs("ur5_robot_moveit_config")
