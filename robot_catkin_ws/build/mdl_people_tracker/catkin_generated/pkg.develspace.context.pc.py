# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/tania/Documents/ur5_robot/robot_catkin_ws/devel/include;/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/include".split(';') if "/home/tania/Documents/ur5_robot/robot_catkin_ws/devel/include;/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/include" != "" else []
PROJECT_CATKIN_DEPENDS = "cv_bridge;geometry_msgs;ground_plane_estimation;image_transport;message_filters;roscpp;sensor_msgs;std_msgs;tf;upper_body_detector;visualization_msgs;visual_odometry".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "mdl_people_tracker"
PROJECT_SPACE_DIR = "/home/tania/Documents/ur5_robot/robot_catkin_ws/devel"
PROJECT_VERSION = "1.9.0"
