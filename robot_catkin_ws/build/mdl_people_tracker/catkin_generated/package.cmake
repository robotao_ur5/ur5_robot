set(_CATKIN_CURRENT_PACKAGE "mdl_people_tracker")
set(mdl_people_tracker_VERSION "1.9.0")
set(mdl_people_tracker_MAINTAINER "Marc Hanheide <marc@hanheide.net>")
set(mdl_people_tracker_PACKAGE_FORMAT "1")
set(mdl_people_tracker_BUILD_DEPENDS "boost" "cv_bridge" "geometry_msgs" "ground_plane_estimation" "image_transport" "libqt4-dev" "message_filters" "message_generation" "roscpp" "sensor_msgs" "std_msgs" "tf" "upper_body_detector" "visualization_msgs" "visual_odometry")
set(mdl_people_tracker_BUILD_EXPORT_DEPENDS "boost" "cv_bridge" "geometry_msgs" "ground_plane_estimation" "image_transport" "libqt4-dev" "message_filters" "message_runtime" "roscpp" "sensor_msgs" "std_msgs" "tf" "upper_body_detector" "visualization_msgs" "visual_odometry")
set(mdl_people_tracker_BUILDTOOL_DEPENDS "catkin")
set(mdl_people_tracker_BUILDTOOL_EXPORT_DEPENDS )
set(mdl_people_tracker_EXEC_DEPENDS "boost" "cv_bridge" "geometry_msgs" "ground_plane_estimation" "image_transport" "libqt4-dev" "message_filters" "message_runtime" "roscpp" "sensor_msgs" "std_msgs" "tf" "upper_body_detector" "visualization_msgs" "visual_odometry")
set(mdl_people_tracker_RUN_DEPENDS "boost" "cv_bridge" "geometry_msgs" "ground_plane_estimation" "image_transport" "libqt4-dev" "message_filters" "message_runtime" "roscpp" "sensor_msgs" "std_msgs" "tf" "upper_body_detector" "visualization_msgs" "visual_odometry")
set(mdl_people_tracker_TEST_DEPENDS )
set(mdl_people_tracker_DOC_DEPENDS )
set(mdl_people_tracker_URL_WEBSITE "https://github.com/strands-project/strands_perception_people")
set(mdl_people_tracker_URL_BUGTRACKER "")
set(mdl_people_tracker_URL_REPOSITORY "")
set(mdl_people_tracker_DEPRECATED "")