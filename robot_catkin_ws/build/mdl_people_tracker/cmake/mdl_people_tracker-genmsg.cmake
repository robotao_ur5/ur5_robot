# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "mdl_people_tracker: 4 messages, 0 services")

set(MSG_I_FLAGS "-Imdl_people_tracker:/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/melodic/share/geometry_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(mdl_people_tracker_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg" NAME_WE)
add_custom_target(_mdl_people_tracker_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "mdl_people_tracker" "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg" "mdl_people_tracker/MdlPeopleTracker:std_msgs/Header"
)

get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg" NAME_WE)
add_custom_target(_mdl_people_tracker_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "mdl_people_tracker" "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg" NAME_WE)
add_custom_target(_mdl_people_tracker_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "mdl_people_tracker" "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg" "mdl_people_tracker/TrackedPerson2d:std_msgs/Header"
)

get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg" NAME_WE)
add_custom_target(_mdl_people_tracker_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "mdl_people_tracker" "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_cpp(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_cpp(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_cpp(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mdl_people_tracker
)

### Generating Services

### Generating Module File
_generate_module_cpp(mdl_people_tracker
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mdl_people_tracker
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(mdl_people_tracker_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(mdl_people_tracker_generate_messages mdl_people_tracker_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_cpp _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_cpp _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_cpp _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_cpp _mdl_people_tracker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mdl_people_tracker_gencpp)
add_dependencies(mdl_people_tracker_gencpp mdl_people_tracker_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mdl_people_tracker_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_eus(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_eus(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_eus(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mdl_people_tracker
)

### Generating Services

### Generating Module File
_generate_module_eus(mdl_people_tracker
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mdl_people_tracker
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(mdl_people_tracker_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(mdl_people_tracker_generate_messages mdl_people_tracker_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_eus _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_eus _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_eus _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_eus _mdl_people_tracker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mdl_people_tracker_geneus)
add_dependencies(mdl_people_tracker_geneus mdl_people_tracker_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mdl_people_tracker_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_lisp(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_lisp(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_lisp(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mdl_people_tracker
)

### Generating Services

### Generating Module File
_generate_module_lisp(mdl_people_tracker
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mdl_people_tracker
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(mdl_people_tracker_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(mdl_people_tracker_generate_messages mdl_people_tracker_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_lisp _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_lisp _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_lisp _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_lisp _mdl_people_tracker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mdl_people_tracker_genlisp)
add_dependencies(mdl_people_tracker_genlisp mdl_people_tracker_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mdl_people_tracker_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_nodejs(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_nodejs(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_nodejs(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mdl_people_tracker
)

### Generating Services

### Generating Module File
_generate_module_nodejs(mdl_people_tracker
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mdl_people_tracker
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(mdl_people_tracker_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(mdl_people_tracker_generate_messages mdl_people_tracker_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_nodejs _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_nodejs _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_nodejs _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_nodejs _mdl_people_tracker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mdl_people_tracker_gennodejs)
add_dependencies(mdl_people_tracker_gennodejs mdl_people_tracker_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mdl_people_tracker_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_py(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_py(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg"
  "${MSG_I_FLAGS}"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mdl_people_tracker
)
_generate_msg_py(mdl_people_tracker
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mdl_people_tracker
)

### Generating Services

### Generating Module File
_generate_module_py(mdl_people_tracker
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mdl_people_tracker
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(mdl_people_tracker_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(mdl_people_tracker_generate_messages mdl_people_tracker_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTrackerArray.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_py _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/MdlPeopleTracker.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_py _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPersons2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_py _mdl_people_tracker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/msg/TrackedPerson2d.msg" NAME_WE)
add_dependencies(mdl_people_tracker_generate_messages_py _mdl_people_tracker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(mdl_people_tracker_genpy)
add_dependencies(mdl_people_tracker_genpy mdl_people_tracker_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS mdl_people_tracker_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mdl_people_tracker)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/mdl_people_tracker
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(mdl_people_tracker_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(mdl_people_tracker_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mdl_people_tracker)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/mdl_people_tracker
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(mdl_people_tracker_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(mdl_people_tracker_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mdl_people_tracker)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/mdl_people_tracker
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(mdl_people_tracker_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(mdl_people_tracker_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mdl_people_tracker)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/mdl_people_tracker
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(mdl_people_tracker_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(mdl_people_tracker_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mdl_people_tracker)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mdl_people_tracker\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/mdl_people_tracker
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(mdl_people_tracker_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(mdl_people_tracker_generate_messages_py geometry_msgs_generate_messages_py)
endif()
