# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/AncillaryMethods.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/AncillaryMethods.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/Camera.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/Camera.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/Detections.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/Detections.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/EKalman.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/EKalman.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/FrameInlier.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/FrameInlier.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/Globals.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/Globals.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/Hypo.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/Hypo.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/Kalman.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/Kalman.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/MDL.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/MDL.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/Math.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/Math.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/Tracker.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/Tracker.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/Visualization.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/Visualization.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/src/main.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/mdl_people_tracker/CMakeFiles/mdl_people_tracker.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"mdl_people_tracker\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/devel/include"
  "/usr/include/qt4"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/mdl_people_tracker/include"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/ground_plane_estimation/include"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
