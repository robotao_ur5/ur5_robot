set(_CATKIN_CURRENT_PACKAGE "upper_body_detector")
set(upper_body_detector_VERSION "1.9.0")
set(upper_body_detector_MAINTAINER "Marc Hanheide <marc@hanheide.net>")
set(upper_body_detector_PACKAGE_FORMAT "1")
set(upper_body_detector_BUILD_DEPENDS "boost" "cv_bridge" "geometry_msgs" "ground_plane_estimation" "image_transport" "libqt4-dev" "message_filters" "message_generation" "roscpp" "sensor_msgs" "std_msgs" "visualization_msgs")
set(upper_body_detector_BUILD_EXPORT_DEPENDS "boost" "cv_bridge" "geometry_msgs" "ground_plane_estimation" "image_transport" "libqt4-dev" "message_filters" "message_runtime" "roscpp" "sensor_msgs" "std_msgs" "visualization_msgs")
set(upper_body_detector_BUILDTOOL_DEPENDS "catkin")
set(upper_body_detector_BUILDTOOL_EXPORT_DEPENDS )
set(upper_body_detector_EXEC_DEPENDS "boost" "cv_bridge" "geometry_msgs" "ground_plane_estimation" "image_transport" "libqt4-dev" "message_filters" "message_runtime" "roscpp" "sensor_msgs" "std_msgs" "visualization_msgs")
set(upper_body_detector_RUN_DEPENDS "boost" "cv_bridge" "geometry_msgs" "ground_plane_estimation" "image_transport" "libqt4-dev" "message_filters" "message_runtime" "roscpp" "sensor_msgs" "std_msgs" "visualization_msgs")
set(upper_body_detector_TEST_DEPENDS )
set(upper_body_detector_DOC_DEPENDS )
set(upper_body_detector_URL_WEBSITE "https://github.com/strands-project/strands_perception_people")
set(upper_body_detector_URL_BUGTRACKER "")
set(upper_body_detector_URL_REPOSITORY "")
set(upper_body_detector_DEPRECATED "")