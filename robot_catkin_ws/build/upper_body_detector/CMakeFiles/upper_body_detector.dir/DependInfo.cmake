# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/AncillaryMethods.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/AncillaryMethods.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/CPoint.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/CPoint.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/Camera.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/Camera.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/FrameInlier.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/FrameInlier.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/Globals.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/Globals.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/Hypo.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/Hypo.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/KConnectedComponentLabeler.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/KConnectedComponentLabeler.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/Math.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/Math.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/ROI.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/ROI.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/detector.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/detector.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/main.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/main.cpp.o"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/src/pointcloud.cpp" "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/upper_body_detector/CMakeFiles/upper_body_detector.dir/src/pointcloud.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"upper_body_detector\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/devel/include"
  "/usr/include/qt4"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/upper_body_detector/include"
  "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/ground_plane_estimation/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
