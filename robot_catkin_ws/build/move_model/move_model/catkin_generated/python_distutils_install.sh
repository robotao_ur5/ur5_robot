#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/move_model/move_model"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/tania/Documents/ur5_robot/robot_catkin_ws/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/tania/Documents/ur5_robot/robot_catkin_ws/install/lib/python2.7/dist-packages:/home/tania/Documents/ur5_robot/robot_catkin_ws/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/tania/Documents/ur5_robot/robot_catkin_ws/build" \
    "/usr/bin/python2" \
    "/home/tania/Documents/ur5_robot/robot_catkin_ws/src/move_model/move_model/setup.py" \
     \
    build --build-base "/home/tania/Documents/ur5_robot/robot_catkin_ws/build/move_model/move_model" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/tania/Documents/ur5_robot/robot_catkin_ws/install" --install-scripts="/home/tania/Documents/ur5_robot/robot_catkin_ws/install/bin"
