
(cl:in-package :asdf)

(defsystem "ground_plane_estimation-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "GroundPlane" :depends-on ("_package_GroundPlane"))
    (:file "_package_GroundPlane" :depends-on ("_package"))
  ))