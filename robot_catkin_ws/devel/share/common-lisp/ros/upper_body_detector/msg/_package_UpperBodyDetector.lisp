(cl:in-package upper_body_detector-msg)
(cl:export '(HEADER-VAL
          HEADER
          POS_X-VAL
          POS_X
          POS_Y-VAL
          POS_Y
          WIDTH-VAL
          WIDTH
          HEIGHT-VAL
          HEIGHT
          DIST-VAL
          DIST
          MEDIAN_DEPTH-VAL
          MEDIAN_DEPTH
))