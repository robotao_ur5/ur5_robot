
(cl:in-package :asdf)

(defsystem "upper_body_detector-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "UpperBodyDetector" :depends-on ("_package_UpperBodyDetector"))
    (:file "_package_UpperBodyDetector" :depends-on ("_package"))
  ))