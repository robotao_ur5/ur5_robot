
(cl:in-package :asdf)

(defsystem "visual_odometry-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "VisualOdometry" :depends-on ("_package_VisualOdometry"))
    (:file "_package_VisualOdometry" :depends-on ("_package"))
  ))