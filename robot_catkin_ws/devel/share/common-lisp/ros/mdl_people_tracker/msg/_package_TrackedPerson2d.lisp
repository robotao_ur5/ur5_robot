(cl:in-package mdl_people_tracker-msg)
(cl:export '(TRACK_ID-VAL
          TRACK_ID
          PERSON_HEIGHT-VAL
          PERSON_HEIGHT
          X-VAL
          X
          Y-VAL
          Y
          W-VAL
          W
          H-VAL
          H
          DEPTH-VAL
          DEPTH
))