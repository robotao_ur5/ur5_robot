; Auto-generated. Do not edit!


(cl:in-package mdl_people_tracker-msg)


;//! \htmlinclude MdlPeopleTrackerArray.msg.html

(cl:defclass <MdlPeopleTrackerArray> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (people
    :reader people
    :initarg :people
    :type (cl:vector mdl_people_tracker-msg:MdlPeopleTracker)
   :initform (cl:make-array 0 :element-type 'mdl_people_tracker-msg:MdlPeopleTracker :initial-element (cl:make-instance 'mdl_people_tracker-msg:MdlPeopleTracker))))
)

(cl:defclass MdlPeopleTrackerArray (<MdlPeopleTrackerArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MdlPeopleTrackerArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MdlPeopleTrackerArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name mdl_people_tracker-msg:<MdlPeopleTrackerArray> is deprecated: use mdl_people_tracker-msg:MdlPeopleTrackerArray instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MdlPeopleTrackerArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader mdl_people_tracker-msg:header-val is deprecated.  Use mdl_people_tracker-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'people-val :lambda-list '(m))
(cl:defmethod people-val ((m <MdlPeopleTrackerArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader mdl_people_tracker-msg:people-val is deprecated.  Use mdl_people_tracker-msg:people instead.")
  (people m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MdlPeopleTrackerArray>) ostream)
  "Serializes a message object of type '<MdlPeopleTrackerArray>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'people))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'people))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MdlPeopleTrackerArray>) istream)
  "Deserializes a message object of type '<MdlPeopleTrackerArray>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'people) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'people)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'mdl_people_tracker-msg:MdlPeopleTracker))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MdlPeopleTrackerArray>)))
  "Returns string type for a message object of type '<MdlPeopleTrackerArray>"
  "mdl_people_tracker/MdlPeopleTrackerArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MdlPeopleTrackerArray)))
  "Returns string type for a message object of type 'MdlPeopleTrackerArray"
  "mdl_people_tracker/MdlPeopleTrackerArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MdlPeopleTrackerArray>)))
  "Returns md5sum for a message object of type '<MdlPeopleTrackerArray>"
  "2621953c2362560959886c137848a6c7")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MdlPeopleTrackerArray)))
  "Returns md5sum for a message object of type 'MdlPeopleTrackerArray"
  "2621953c2362560959886c137848a6c7")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MdlPeopleTrackerArray>)))
  "Returns full string definition for message of type '<MdlPeopleTrackerArray>"
  (cl:format cl:nil "Header header~%MdlPeopleTracker[] people~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: mdl_people_tracker/MdlPeopleTracker~%Header header~%# position projected on the GP in world coordinates~%float64[] traj_x ~%float64[] traj_y ~%float64[] traj_z~%# position projected on the GP in camera coordinates~%float64[] traj_x_camera ~%float64[] traj_y_camera ~%float64[] traj_z_camera ~%float64[] dir           # Orientation of person for every point in traj~%float64 speed           # Speed of detected person~%int64 id                # Tracking id. Will be reset to 0 after restart~%string uuid             # Unique uuid5 (NAMESPACE_DNS) person id as string. Id is based on system time on start-up and id.~%float64 score           # Tracker confidence~%uint32[] seq            # Seq. numbers from the headers of UBD messages. These identify the UBD messages that were used for a tracker hypothesis.~%int32[] index           # Index for the arrays in a UBD message.~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MdlPeopleTrackerArray)))
  "Returns full string definition for message of type 'MdlPeopleTrackerArray"
  (cl:format cl:nil "Header header~%MdlPeopleTracker[] people~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: mdl_people_tracker/MdlPeopleTracker~%Header header~%# position projected on the GP in world coordinates~%float64[] traj_x ~%float64[] traj_y ~%float64[] traj_z~%# position projected on the GP in camera coordinates~%float64[] traj_x_camera ~%float64[] traj_y_camera ~%float64[] traj_z_camera ~%float64[] dir           # Orientation of person for every point in traj~%float64 speed           # Speed of detected person~%int64 id                # Tracking id. Will be reset to 0 after restart~%string uuid             # Unique uuid5 (NAMESPACE_DNS) person id as string. Id is based on system time on start-up and id.~%float64 score           # Tracker confidence~%uint32[] seq            # Seq. numbers from the headers of UBD messages. These identify the UBD messages that were used for a tracker hypothesis.~%int32[] index           # Index for the arrays in a UBD message.~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MdlPeopleTrackerArray>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'people) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MdlPeopleTrackerArray>))
  "Converts a ROS message object to a list"
  (cl:list 'MdlPeopleTrackerArray
    (cl:cons ':header (header msg))
    (cl:cons ':people (people msg))
))
