
(cl:in-package :asdf)

(defsystem "mdl_people_tracker-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "MdlPeopleTracker" :depends-on ("_package_MdlPeopleTracker"))
    (:file "_package_MdlPeopleTracker" :depends-on ("_package"))
    (:file "MdlPeopleTrackerArray" :depends-on ("_package_MdlPeopleTrackerArray"))
    (:file "_package_MdlPeopleTrackerArray" :depends-on ("_package"))
    (:file "TrackedPerson2d" :depends-on ("_package_TrackedPerson2d"))
    (:file "_package_TrackedPerson2d" :depends-on ("_package"))
    (:file "TrackedPersons2d" :depends-on ("_package_TrackedPersons2d"))
    (:file "_package_TrackedPersons2d" :depends-on ("_package"))
  ))