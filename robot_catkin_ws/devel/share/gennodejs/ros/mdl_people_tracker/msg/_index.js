
"use strict";

let TrackedPerson2d = require('./TrackedPerson2d.js');
let MdlPeopleTrackerArray = require('./MdlPeopleTrackerArray.js');
let MdlPeopleTracker = require('./MdlPeopleTracker.js');
let TrackedPersons2d = require('./TrackedPersons2d.js');

module.exports = {
  TrackedPerson2d: TrackedPerson2d,
  MdlPeopleTrackerArray: MdlPeopleTrackerArray,
  MdlPeopleTracker: MdlPeopleTracker,
  TrackedPersons2d: TrackedPersons2d,
};
