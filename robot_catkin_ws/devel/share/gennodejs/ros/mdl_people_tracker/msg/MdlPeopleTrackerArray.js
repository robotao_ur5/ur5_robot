// Auto-generated. Do not edit!

// (in-package mdl_people_tracker.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let MdlPeopleTracker = require('./MdlPeopleTracker.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class MdlPeopleTrackerArray {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.people = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('people')) {
        this.people = initObj.people
      }
      else {
        this.people = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MdlPeopleTrackerArray
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [people]
    // Serialize the length for message field [people]
    bufferOffset = _serializer.uint32(obj.people.length, buffer, bufferOffset);
    obj.people.forEach((val) => {
      bufferOffset = MdlPeopleTracker.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MdlPeopleTrackerArray
    let len;
    let data = new MdlPeopleTrackerArray(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [people]
    // Deserialize array length for message field [people]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.people = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.people[i] = MdlPeopleTracker.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    object.people.forEach((val) => {
      length += MdlPeopleTracker.getMessageSize(val);
    });
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'mdl_people_tracker/MdlPeopleTrackerArray';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '2621953c2362560959886c137848a6c7';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    MdlPeopleTracker[] people
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: mdl_people_tracker/MdlPeopleTracker
    Header header
    # position projected on the GP in world coordinates
    float64[] traj_x 
    float64[] traj_y 
    float64[] traj_z
    # position projected on the GP in camera coordinates
    float64[] traj_x_camera 
    float64[] traj_y_camera 
    float64[] traj_z_camera 
    float64[] dir           # Orientation of person for every point in traj
    float64 speed           # Speed of detected person
    int64 id                # Tracking id. Will be reset to 0 after restart
    string uuid             # Unique uuid5 (NAMESPACE_DNS) person id as string. Id is based on system time on start-up and id.
    float64 score           # Tracker confidence
    uint32[] seq            # Seq. numbers from the headers of UBD messages. These identify the UBD messages that were used for a tracker hypothesis.
    int32[] index           # Index for the arrays in a UBD message.
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MdlPeopleTrackerArray(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.people !== undefined) {
      resolved.people = new Array(msg.people.length);
      for (let i = 0; i < resolved.people.length; ++i) {
        resolved.people[i] = MdlPeopleTracker.Resolve(msg.people[i]);
      }
    }
    else {
      resolved.people = []
    }

    return resolved;
    }
};

module.exports = MdlPeopleTrackerArray;
