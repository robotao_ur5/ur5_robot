;; Auto-generated. Do not edit!


(when (boundp 'mdl_people_tracker::MdlPeopleTracker)
  (if (not (find-package "MDL_PEOPLE_TRACKER"))
    (make-package "MDL_PEOPLE_TRACKER"))
  (shadow 'MdlPeopleTracker (find-package "MDL_PEOPLE_TRACKER")))
(unless (find-package "MDL_PEOPLE_TRACKER::MDLPEOPLETRACKER")
  (make-package "MDL_PEOPLE_TRACKER::MDLPEOPLETRACKER"))

(in-package "ROS")
;;//! \htmlinclude MdlPeopleTracker.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass mdl_people_tracker::MdlPeopleTracker
  :super ros::object
  :slots (_header _traj_x _traj_y _traj_z _traj_x_camera _traj_y_camera _traj_z_camera _dir _speed _id _uuid _score _seq _index ))

(defmethod mdl_people_tracker::MdlPeopleTracker
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:traj_x __traj_x) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:traj_y __traj_y) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:traj_z __traj_z) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:traj_x_camera __traj_x_camera) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:traj_y_camera __traj_y_camera) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:traj_z_camera __traj_z_camera) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:dir __dir) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:speed __speed) 0.0)
    ((:id __id) 0)
    ((:uuid __uuid) "")
    ((:score __score) 0.0)
    ((:seq __seq) (make-array 0 :initial-element 0 :element-type :integer))
    ((:index __index) (make-array 0 :initial-element 0 :element-type :integer))
    )
   (send-super :init)
   (setq _header __header)
   (setq _traj_x __traj_x)
   (setq _traj_y __traj_y)
   (setq _traj_z __traj_z)
   (setq _traj_x_camera __traj_x_camera)
   (setq _traj_y_camera __traj_y_camera)
   (setq _traj_z_camera __traj_z_camera)
   (setq _dir __dir)
   (setq _speed (float __speed))
   (setq _id (round __id))
   (setq _uuid (string __uuid))
   (setq _score (float __score))
   (setq _seq __seq)
   (setq _index __index)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:traj_x
   (&optional __traj_x)
   (if __traj_x (setq _traj_x __traj_x)) _traj_x)
  (:traj_y
   (&optional __traj_y)
   (if __traj_y (setq _traj_y __traj_y)) _traj_y)
  (:traj_z
   (&optional __traj_z)
   (if __traj_z (setq _traj_z __traj_z)) _traj_z)
  (:traj_x_camera
   (&optional __traj_x_camera)
   (if __traj_x_camera (setq _traj_x_camera __traj_x_camera)) _traj_x_camera)
  (:traj_y_camera
   (&optional __traj_y_camera)
   (if __traj_y_camera (setq _traj_y_camera __traj_y_camera)) _traj_y_camera)
  (:traj_z_camera
   (&optional __traj_z_camera)
   (if __traj_z_camera (setq _traj_z_camera __traj_z_camera)) _traj_z_camera)
  (:dir
   (&optional __dir)
   (if __dir (setq _dir __dir)) _dir)
  (:speed
   (&optional __speed)
   (if __speed (setq _speed __speed)) _speed)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:uuid
   (&optional __uuid)
   (if __uuid (setq _uuid __uuid)) _uuid)
  (:score
   (&optional __score)
   (if __score (setq _score __score)) _score)
  (:seq
   (&optional __seq)
   (if __seq (setq _seq __seq)) _seq)
  (:index
   (&optional __index)
   (if __index (setq _index __index)) _index)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float64[] _traj_x
    (* 8    (length _traj_x)) 4
    ;; float64[] _traj_y
    (* 8    (length _traj_y)) 4
    ;; float64[] _traj_z
    (* 8    (length _traj_z)) 4
    ;; float64[] _traj_x_camera
    (* 8    (length _traj_x_camera)) 4
    ;; float64[] _traj_y_camera
    (* 8    (length _traj_y_camera)) 4
    ;; float64[] _traj_z_camera
    (* 8    (length _traj_z_camera)) 4
    ;; float64[] _dir
    (* 8    (length _dir)) 4
    ;; float64 _speed
    8
    ;; int64 _id
    8
    ;; string _uuid
    4 (length _uuid)
    ;; float64 _score
    8
    ;; uint32[] _seq
    (* 4    (length _seq)) 4
    ;; int32[] _index
    (* 4    (length _index)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float64[] _traj_x
     (write-long (length _traj_x) s)
     (dotimes (i (length _traj_x))
       (sys::poke (elt _traj_x i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _traj_y
     (write-long (length _traj_y) s)
     (dotimes (i (length _traj_y))
       (sys::poke (elt _traj_y i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _traj_z
     (write-long (length _traj_z) s)
     (dotimes (i (length _traj_z))
       (sys::poke (elt _traj_z i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _traj_x_camera
     (write-long (length _traj_x_camera) s)
     (dotimes (i (length _traj_x_camera))
       (sys::poke (elt _traj_x_camera i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _traj_y_camera
     (write-long (length _traj_y_camera) s)
     (dotimes (i (length _traj_y_camera))
       (sys::poke (elt _traj_y_camera i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _traj_z_camera
     (write-long (length _traj_z_camera) s)
     (dotimes (i (length _traj_z_camera))
       (sys::poke (elt _traj_z_camera i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64[] _dir
     (write-long (length _dir) s)
     (dotimes (i (length _dir))
       (sys::poke (elt _dir i) (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
       )
     ;; float64 _speed
       (sys::poke _speed (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; int64 _id
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _id (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _id) (= (length (_id . bv)) 2)) ;; bignum
              (write-long (ash (elt (_id . bv) 0) 0) s)
              (write-long (ash (elt (_id . bv) 1) -1) s))
             ((and (class _id) (= (length (_id . bv)) 1)) ;; big1
              (write-long (elt (_id . bv) 0) s)
              (write-long (if (>= _id 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _id s)(write-long (if (>= _id 0) 0 #xffffffff) s)))
     ;; string _uuid
       (write-long (length _uuid) s) (princ _uuid s)
     ;; float64 _score
       (sys::poke _score (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; uint32[] _seq
     (write-long (length _seq) s)
     (dotimes (i (length _seq))
       (write-long (elt _seq i) s)
       )
     ;; int32[] _index
     (write-long (length _index) s)
     (dotimes (i (length _index))
       (write-long (elt _index i) s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float64[] _traj_x
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _traj_x (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _traj_x i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _traj_y
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _traj_y (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _traj_y i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _traj_z
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _traj_z (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _traj_z i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _traj_x_camera
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _traj_x_camera (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _traj_x_camera i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _traj_y_camera
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _traj_y_camera (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _traj_y_camera i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _traj_z_camera
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _traj_z_camera (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _traj_z_camera i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64[] _dir
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _dir (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _dir i) (sys::peek buf ptr- :double)) (incf ptr- 8)
     ))
   ;; float64 _speed
     (setq _speed (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; int64 _id
#+(or :alpha :irix6 :x86_64)
      (setf _id (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _id (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; string _uuid
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _uuid (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float64 _score
     (setq _score (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; uint32[] _seq
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _seq (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _seq i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; int32[] _index
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _index (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _index i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;;
   self)
  )

(setf (get mdl_people_tracker::MdlPeopleTracker :md5sum-) "c168ab9163478ffb1ca29b0b8e2572cf")
(setf (get mdl_people_tracker::MdlPeopleTracker :datatype-) "mdl_people_tracker/MdlPeopleTracker")
(setf (get mdl_people_tracker::MdlPeopleTracker :definition-)
      "Header header
# position projected on the GP in world coordinates
float64[] traj_x 
float64[] traj_y 
float64[] traj_z
# position projected on the GP in camera coordinates
float64[] traj_x_camera 
float64[] traj_y_camera 
float64[] traj_z_camera 
float64[] dir           # Orientation of person for every point in traj
float64 speed           # Speed of detected person
int64 id                # Tracking id. Will be reset to 0 after restart
string uuid             # Unique uuid5 (NAMESPACE_DNS) person id as string. Id is based on system time on start-up and id.
float64 score           # Tracker confidence
uint32[] seq            # Seq. numbers from the headers of UBD messages. These identify the UBD messages that were used for a tracker hypothesis.
int32[] index           # Index for the arrays in a UBD message.
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :mdl_people_tracker/MdlPeopleTracker "c168ab9163478ffb1ca29b0b8e2572cf")


