;; Auto-generated. Do not edit!


(when (boundp 'mdl_people_tracker::MdlPeopleTrackerArray)
  (if (not (find-package "MDL_PEOPLE_TRACKER"))
    (make-package "MDL_PEOPLE_TRACKER"))
  (shadow 'MdlPeopleTrackerArray (find-package "MDL_PEOPLE_TRACKER")))
(unless (find-package "MDL_PEOPLE_TRACKER::MDLPEOPLETRACKERARRAY")
  (make-package "MDL_PEOPLE_TRACKER::MDLPEOPLETRACKERARRAY"))

(in-package "ROS")
;;//! \htmlinclude MdlPeopleTrackerArray.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass mdl_people_tracker::MdlPeopleTrackerArray
  :super ros::object
  :slots (_header _people ))

(defmethod mdl_people_tracker::MdlPeopleTrackerArray
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:people __people) (let (r) (dotimes (i 0) (push (instance mdl_people_tracker::MdlPeopleTracker :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _people __people)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:people
   (&rest __people)
   (if (keywordp (car __people))
       (send* _people __people)
     (progn
       (if __people (setq _people (car __people)))
       _people)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; mdl_people_tracker/MdlPeopleTracker[] _people
    (apply #'+ (send-all _people :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; mdl_people_tracker/MdlPeopleTracker[] _people
     (write-long (length _people) s)
     (dolist (elem _people)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; mdl_people_tracker/MdlPeopleTracker[] _people
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _people (let (r) (dotimes (i n) (push (instance mdl_people_tracker::MdlPeopleTracker :init) r)) r))
     (dolist (elem- _people)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get mdl_people_tracker::MdlPeopleTrackerArray :md5sum-) "2621953c2362560959886c137848a6c7")
(setf (get mdl_people_tracker::MdlPeopleTrackerArray :datatype-) "mdl_people_tracker/MdlPeopleTrackerArray")
(setf (get mdl_people_tracker::MdlPeopleTrackerArray :definition-)
      "Header header
MdlPeopleTracker[] people

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: mdl_people_tracker/MdlPeopleTracker
Header header
# position projected on the GP in world coordinates
float64[] traj_x 
float64[] traj_y 
float64[] traj_z
# position projected on the GP in camera coordinates
float64[] traj_x_camera 
float64[] traj_y_camera 
float64[] traj_z_camera 
float64[] dir           # Orientation of person for every point in traj
float64 speed           # Speed of detected person
int64 id                # Tracking id. Will be reset to 0 after restart
string uuid             # Unique uuid5 (NAMESPACE_DNS) person id as string. Id is based on system time on start-up and id.
float64 score           # Tracker confidence
uint32[] seq            # Seq. numbers from the headers of UBD messages. These identify the UBD messages that were used for a tracker hypothesis.
int32[] index           # Index for the arrays in a UBD message.
")



(provide :mdl_people_tracker/MdlPeopleTrackerArray "2621953c2362560959886c137848a6c7")


