#!/usr/bin/env python
#######################
# Description: Script for actuating the movement of the UR5 robot
# Author: Tania-Andreea Grama
# Part of: RoboTAO - Smart Solution Semester
# Inspired from: http://docs.ros.org/kinetic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html
#######################
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from geometry_msgs.msg import PoseStamped
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


class MoveRobotDemo(object):

  def __init__(self):
    super(MoveRobotDemo, self).__init__()
    # initialize "moveit_commander" and a "rospy" node
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('robot_move',
                    anonymous=True)

    # RobotCommander object
    scene = moveit_commander.PlanningSceneInterface()
    robot = moveit_commander.RobotCommander()

    # adding box 
    # rospy.sleep(2)

    # p = PoseStamped()
    # p.header.frame_id = robot.get_planning_frame()
    # p.pose.position.x = 0.7
    # p.pose.position.y = -0.2
    # p.pose.position.z = 0.
    # scene.add_box("box", p, (0.2, 0.2, 0.2))

    # `MoveGroupCommander` object (in this case it is set to the group_name defined in Moveit) 
    group_name = "manipulator"
    group = moveit_commander.MoveGroupCommander(group_name)
    #`DisplayTrajectory` publisher ( used later to publish trajectories for RViz)
    display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                   moveit_msgs.msg.DisplayTrajectory,
                                                   queue_size=20)
                                                   
    # get the name of the reference frame for the robot
    planning_frame = group.get_planning_frame()

    # # print the name of the end-effector link for the group
    eef_link = group.get_end_effector_link()

    # get a list of all the groups in the robot:
    group_names = robot.get_group_names()

    # print entire state of the robot (debugging purposes)
    print "**** Printing robot state ****"
    print robot.get_current_state()
    print ""

    # misc variables
    self.box_name = ''
    self.robot = robot
    self.scene = scene
    self.group = group
    self.display_trajectory_publisher = display_trajectory_publisher
    self.planning_frame = planning_frame
    self.eef_link = eef_link
    self.group_names = group_names


  def plan_joints_goal(self, scale=1):
    group = self.group
    joint_goal = group.get_current_joint_values()
    joint_goal[0] = -1.5
    joint_goal[1] = -1.5
    joint_goal[2] = 1.5
    group.go(joint_goal, wait=False)
    # calling ``stop()`` ensures that there is no residual movement
    group.stop()
    current_joints = self.group.get_current_joint_values()
   

  def plan_cartesian_path(self, scale=1):
    # specifying a list of waypoints for the end-effector to go through
    waypoints = []

    wpose = group.get_current_pose().pose
    # move up
    wpose.position.z += scale * 2
    waypoints.append(copy.deepcopy(wpose))
    (plan, fraction) = group.compute_cartesian_path(
                                       waypoints,   # waypoints to follow
                                       0.01,        # eef_step
                                       0.0)         # jump_threshold
    return plan, fraction
  
  def execute_plan(self, plan):
    group = self.group 
    #robot follows the plan that has already been computed
    group.execute(plan, wait=False)


def main():
  try:
    print "**** Press `Enter` to begin the demo or press ctrl-d to exit ****"
    raw_input()
    demo = MoveRobotDemo()
    demo.plan_joints_goal()
    
    print "**** Demo complete! ****"
  except rospy.ROSInterruptException:
    return
  except KeyboardInterrupt:
    return

if __name__ == '__main__':
  main()

