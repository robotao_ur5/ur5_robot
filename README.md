# Steps to set up and run the project

### 1. Clone and fetch this repository

#### 1.1. Go to the folder where you want to clone the repository 
``` console
cd my_folder
```
#### 1.2. Clone the repository
``` console
git clone https://taniagrama@bitbucket.org/robotao_ur5/ur5_robot.git
```

### 2. Build the code in the catkin workspace (*make sure that you are in the ur5_robot folder*)
``` console
cd robot_catkin_ws
source ./devel/setup.bash
```
### 3. Add project's environment variables to bash
```
echo "source ~/Documents/ur5_robot/robot_catkin_ws/devel/setup.bash" >> ~/.bashrc
```
```
source ~/.bshrc
```
### 4. Launch the Gazebo environment (*open one more terminal, do not close the previous one*)
``` console
roslaunch ur5_robot_gazebo ur5_gazebo.launch
```
### 5. Launch the Rviz environment (*make sure that you are in the robot_catkin_ws folder*)
``` console
roslaunch ur5_robot_moveit_config obstacle_avoidance.launch
```
*Note: For clearing the octomap “Clear octomap” button from the “Planning” pane can be pressed
### 6. Move human around (the human can be moved in different directions by typing the following commands in the terminal in which the Gazebo simulation was launched)
```
Moving around:
        i    
   j    k    l
        m    

anything else : stop
i : move forward
m : move backward
j : move circularly to the left
l : move circularly to the right
k: stop moving
* The commands can be combined (e.g. i + m)

q/z : increase/decrease max speeds by 10%
w/x : increase/decrease only linear speed by 10%
```
### 7. Run the Python script to move the robot
#### 7.1. Open another terminal (*do not close the other terminals*)
#### 7.2. Run the Python script
``` console
cd your_folder
cd ur5_robot/robot_catkin_ws
rosrun robot_controller control.py
```
After the script started to run, press “Enter” for planning a pre-specified path of the robot
### 8. Test the human detection capability
 ```
 rosrun rviz rviz
 ```  
 *Note: After Rviz opened, for “Fixed Frame” it should be selected the camera_rgb_frame. Then different topics can be added through the Rviz interface. For example, to see how the upper body is detected the topic /upper_body_detector/image should be added, while for testing the human tracker the topic /mdl_people_tracker/image can be used. To be mentioned that the human needs to be closer to the robot, in order to be detected, for the initial position of the human, there will be no detection as it is positioned too far away from the camera.